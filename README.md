Одна ветка -- одно задание

В папке **task** находится всё необходимое по задаче 
(условие, файлы и прочее).

В папке **solution** должно быть решение.
Обязательно наличие файла requirements.txt
